//
//  ViewController.swift
//  ExpenseTracker
//
//  Created by Margaret Schroeder on 10/6/17.
//  Copyright © 2017 Margaret Schroeder. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var tasks: [Payment] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let task = Payment(context: context)
        task.name = "test"
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        getData()
        print(tasks)
    }
    
    func getData() {
        do {
            tasks = try context.fetch(Payment.fetchRequest())
        }
        catch {
            print("Fetching Failed")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
